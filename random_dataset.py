import sklearn.cluster as cluster
import sklearn.manifold as manifold
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.decomposition import PCA
from sklearn.datasets import make_blobs


def embed(_datapoints, _centroids):
    _embedded = []
    _embedded_centroid = []
    for point in _datapoints:
        for centroid in _centroids:
            _embedded_centroid.append(np.linalg.norm(centroid - point))
        _embedded.append(_embedded_centroid)
        _embedded_centroid = []

    return np.array(_embedded)


if __name__ == '__main__':
    centroids_count = 100
    perplexity = 40
    elements = 2000

    # create 100-D random dataset and calculate centroids
    X, y = make_blobs(n_samples=elements, centers=1, n_features=100)

    # print("tSNE of random dataset...")
    # # plot generated dataset with t-SNE
    # original_tsne = manifold.TSNE(n_components=2, perplexity=perplexity).fit_transform(X)
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax.scatter(original_tsne[:, 0], original_tsne[:, 1])
    #
    # plt.title('Random dataset')
    #
    # ax.legend()
    # plt.show()
    #
    # fig.savefig('original_random_dataset.png')

    print("Create centroids...")
    centroids = cluster.KMeans(n_clusters=centroids_count).fit(X).cluster_centers_
    random_centroids_1 = random.sample(list(X), centroids_count)
    random_centroids_2 = random.sample(list(X), centroids_count)
    random_centroids_3 = random.sample(list(X), centroids_count)

    print("Embedding...")
    # get representation after centroids
    embedded = embed(X, centroids)
    embedded_permutation = embed(X, np.random.permutation(centroids))
    embedded_random_1 = embed(X, random_centroids_1)
    embedded_random_2 = embed(X, random_centroids_2)
    embedded_random_3 = embed(X, random_centroids_3)

    X = np.concatenate([embedded, embedded_permutation, embedded_random_1, embedded_random_2, embedded_random_3])
    Y = np.concatenate([[0]*elements, [1]*elements, [2]*elements, [3]*elements, [4]*elements])

    labels = ['k-means', 'permuted k-means', 'random 1', 'random 2', 'random 3']

    print("tSNE embeddings...")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    embedded_tsne = manifold.TSNE(n_components=2, perplexity=perplexity).fit_transform(X)

    for i in range(0, len(labels)):
        ax.scatter(embedded_tsne[:, 0][i*elements:(i+1)*elements],
                   embedded_tsne[:, 1][i*elements:(i+1)*elements],
                   label=labels[i])

    plt.title('tSNE embeddings with different centroids')

    ax.legend()
    plt.show()

    fig.savefig('tsne_embeddings.png')

    print("PCA embeddings...")
    fig = plt.figure()
    ax = fig.add_subplot(111)

    pca = PCA(n_components=2)
    pca_transform = pca.fit_transform(X)

    for i in range(0, len(labels)):
        ax.scatter(pca_transform[:, 0][i * elements:(i + 1) * elements],
                   pca_transform[:, 1][i * elements:(i + 1) * elements],
                   label=labels[i])

    plt.title('PCA embeddings for different centroids')

    ax.legend()
    plt.show()

    fig.savefig('pca_embeddings.png')

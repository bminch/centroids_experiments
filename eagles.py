import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import sklearn.cluster as cluster
import numpy as np
import sklearn.manifold as manifold

crest_1_name = 'austria'
crest_2_name = 'russia'


def embed(_datapoints, _centroids):
    _embedded = []
    _embedded_centroid = []
    for point in _datapoints:
        for centroid in _centroids:
            _embedded_centroid.append(np.linalg.norm(centroid - point))
        _embedded.append(_embedded_centroid)
        _embedded_centroid = []

    return np.array(_embedded)


crest_1 = mpimg.imread('./eagles/{}_crest_grey.bmp'.format(crest_1_name))
crest_2 = mpimg.imread('./eagles/{}_crest_grey.bmp'.format(crest_2_name))

crest_1_coordinates = []
crest_2_coordinates = []

centroids_count = 100

np.savetxt("./eagle.txt", crest_1, fmt='%.0f')

for i in range(0, crest_1.shape[0]):
    for j in range(0, crest_1.shape[1]):
        if crest_1[i][j] != 255:
            crest_1_coordinates.append([i, j])

for i in range(0, crest_2.shape[0]):
    for j in range(0, crest_2.shape[1]):
        if crest_2[i][j] != 255:
            crest_2_coordinates.append([i, j])

crest_1 = np.array(crest_1_coordinates)
crest_2 = np.array(crest_2_coordinates)

print('Calculate centroids 1')
crest_1_centroids = cluster.KMeans(n_clusters=centroids_count).fit(crest_1).cluster_centers_
print('Calculate centroids 2')
crest_2_centroids = cluster.KMeans(n_clusters=centroids_count).fit(crest_2).cluster_centers_

print('Calculate embeddings 1')
embedded_crest_1 = embed(crest_1, crest_1_centroids)
print('Calculate embeddings 2')
embedded_crest_2 = embed(crest_2, crest_2_centroids)

X_original = np.concatenate([crest_1, crest_2])
X = np.concatenate([embedded_crest_1, embedded_crest_2])
Y = np.concatenate([[0] * len(crest_1), [1] * len(crest_2)])

labels = [crest_1_name, crest_2_name]

X_all = np.array([])
# 0 - austria
# 1 - russia
# write(str(len(embedded_crest_1) + len(embedded_crest_2)) + "\n")
# write(str(centroids_count) + "\n")
for i in range(0, len(embedded_crest_1)):
    temp = np.concatenate((embedded_crest_1[i], [0]))
    X_all = np.append(X_all, temp)
for i in range(len(embedded_crest_1), len(embedded_crest_1)+len(embedded_crest_2)):
    temp = np.concatenate((embedded_crest_2[i], [1]))
    X_all = np.append(X_all, temp)

np.savetxt('output.csv', X_all, delimiter=',')

print('Calculate tSNE original')
fig = plt.figure()
ax = fig.add_subplot(111)
embedded_tsne = manifold.TSNE(n_components=2, perplexity=40).fit_transform(X)
original_tsne = manifold.TSNE(n_components=2, perplexity=40).fit_transform(X_original)

for i in range(0, len(labels)):
    if i == 0:
        ax.scatter(original_tsne[:, 0][i * len(crest_1):(i + 1) * len(crest_1)],
                   original_tsne[:, 1][i * len(crest_1):(i + 1) * len(crest_1)],
                   label=labels[i])
    else:
        ax.scatter(original_tsne[:, 0][i * len(crest_1):(i + 1) * len(crest_2)],
                   original_tsne[:, 1][i * len(crest_1):(i + 1) * len(crest_2)],
                   label=labels[i])

fig.savefig('original_eagles_{}_{}.png'.format(crest_1_name, crest_2_name))

print('Calculate tSNE embedded')
fig = plt.figure()
ax = fig.add_subplot(111)

for i in range(0, len(labels)):
    if i == 0:
        ax.scatter(embedded_tsne[:, 0][i * len(crest_1):(i + 1) * len(crest_1)],
                   embedded_tsne[:, 1][i * len(crest_1):(i + 1) * len(crest_1)],
                   label=labels[i])
    else:
        ax.scatter(embedded_tsne[:, 0][i * len(crest_1):(i + 1) * len(crest_2)],
                   embedded_tsne[:, 1][i * len(crest_1):(i + 1) * len(crest_2)],
                   label=labels[i])

ax.legend()
plt.show()

fig.savefig('embedded_eagles_{}_{}.png'.format(crest_1_name, crest_2_name))
